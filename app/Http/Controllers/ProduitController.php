<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Produit;
use Illuminate\Http\Request;


class ProduitController extends Controller
{

    public function ajouter(){
        $catagories = Categorie::all();
        return view('/ajouter',['catagories'=>$catagories]);
    
    }



    public function store(Request $request){
        $p= new Produit();
        $p->nom = $request->nom;
        $p->prix = $request->prix;
        $p->categorie_id = $request->category;
        $p->save();

        
    }


    public function  index(){
        $catagories = Categorie::all();
        return view('/welcome',['catagories'=>$catagories]);
    }
    
    public function  lister($id){
        $produits = Categorie::find($id)->produits;
        return view('list_produit',['produits'=>$produits]);
    }
    
    public function edit($id){
        $categories = Categorie::all();
        $produits = Produit::find($id);

        return view('edit',['produits'=>$produits , 'categories'=>$categories]);

    }

    public function update(Request $request,$id){
        
       // dd($request->prix);
       
        $produits = Produit::find($id);
        $produits->nom = $request->nom;
        $produits->prix = $request->prix;
        
        
        

        $produits->save();
        return redirect(route('racine'));
        

    }
    public function lister_all(){
        $produits = Produit::all();
        return view('list_all_produit',['produits'=>$produits]);
    }
}
