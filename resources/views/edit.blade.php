<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form action="/list_produit/{{$produits->id}}/update" method="post">
        @csrf
        @method('PUT')
        Nom: <br>
        <input type="text" name="nom" value="{{$produits->nom}}"><br>
        Prix: <br>
        <input type="text" name="prix" value="{{$produits->prix}}"> <br>
        
        <select name="category">
            @foreach( $categories as $c)
            @if ($produits->categorie_id == $c->id )
            <option value="{{$c->id}}" selected >{{ $c->nom }}</option>
            @else 
            <option value="{{$c->id}}"  >{{ $c->nom }}</option>
            @endif
            @endforeach
        </select>
        
        <button type="submit">Ajouter</button>
    </form>
</body>
</html>