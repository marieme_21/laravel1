<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>List des produits</h1>
    <table>
        <tr>
            <th>Nom</th>
            <th>Prix</th>
            <th>Categorie</th>
            <th></th>
         
        </tr>
        @foreach ($produits as $produit ) 
        <tr>
        <td>{{$produit->nom}}</td>
            <td>{{$produit->prix}}</td>
            <td>{{$produit->categorie->nom}}</td>

            <td><a href="/list_produit/{{$produit->id}}/edit">modifier</a></td>
       
        </tr>
        @endforeach

    </table>
</body>
</html>