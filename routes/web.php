<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProduitController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/ajouter', [ProduitController::class,'ajouter']);
Route::get('/',[ProduitController::class,'index'])->name('racine');
Route::get('list_produit/{id}',[ProduitController::class,'lister']);
Route::post('/ajouter',[ProduitController::class,'store']);
Route::get("/list_produit/{produit}/edit",[ProduitController::class,'edit']);
Route::put("/list_produit/{id}/update",[ProduitController::class,'update']);
Route::get('list_produit',[ProduitController::class,'lister_all']);
